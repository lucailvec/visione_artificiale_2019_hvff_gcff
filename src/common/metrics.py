from itertools import product
from typing import Union, List

import numpy as np


def evalgroup(groups: List[List[int]], gt: List[List[int]], th: Union[float, str, None] = "card", cardmode : Union[int, None]=None):
    """
    EVALGROUPS computes precision and recall scores, as well as the number of
    true positives, false positives and false negatives, given a set of
    detected groups and a set of ground truth groups.

    :param groups:
    :param gt:
    :param th:
    :param cardmode:
    :return: precision  := the ability to detect 'only' the ground truth (or the
                ability not to generate false positives).
                   Pr = TP / (TP+FP)
  recall     := the ability to detect 'all' the ground truth (or the
                ability not to generate false negatives).
                   Re = TP / (TP+FN)
  TP         := number of True Positives
  FP         := number of False Positives
  FN         := number of False Negatives

 NB: if cardmode is set OFF, all the output are scalars otherwise they
 are all vectors with different sizes depending on the maximum cardinality
 of group and GT inputs.
    """

    groups = [g for g in groups if len(g) >= 2]
    gt = [ g for g in gt if len(g) >= 2]

    if th is None:
        th = 2/3
    elif th =='card':
        th = 2/3
    elif th == 'all':
        th = 1
    else:

        th = float(th)
        if not (0<= th <=1):
            ValueError('TH input parameter cannot be set. It can be either a double between 0 and 1 or a string ''card'' or ''all''.')

    if cardmode is None or cardmode == 0:
        cardmode = 0
    elif cardmode==1:
        cardmode=1
    else:
        ValueError('CARDMODE input parameter cannot be set. It can be either a boolean (0/1) or a string ''cardmode''.')


    if cardmode==0:
        """
        ! tolerant match
        TP detected group (correct)
        FN missdetected group (not over the tolerant match)
        FP hallucinated group (
        """
        # degenerate cases:

        el_group = sum((len(el) for el in groups))
        el_gt_group = sum((len(el) for el in gt))

        if el_group == 0 and el_gt_group == 0:
            TP = 0
            FP = 0
            FN = 0
            precision = 1
            recall = 1
        elif el_group == 0 and el_gt_group > 0 :
            TP = 0
            FP = 0
            FN = el_gt_group
            precision = 1
            recall = 0
        elif el_group > 0 and el_gt_group == 0 :
            TP = 0
            FP = el_group
            FN = 0
            precision = 0
            recall = 1
        else:

            TP = 0 #sum( ( for _g,_gt in product(groups, gt) if corr_detected(_g,_gt) and corr_estimated(_g,_gt)) )

            for gt_group in gt: #for every group groundTruth
                #best_index = np.argmax(( len(set(g).intersection(set(gt_group))) for g in groups))#find the most appropriate group
                #best_grp = groups[best_index]
                for g_group in groups:
                    if corr_detected(g_group,gt_group,th) and corr_estimated(g_group,gt_group,th):
                        TP += 1
                        break #todo questa misura dipende dall'ordine, non dal caso peggiore o migliore

            FP = len(groups) - TP
            FN = len(gt) - TP

            precision = TP / ( TP + FP + np.finfo(float).eps)
            recall = TP / (TP + FN + np.finfo(float).eps    )
    else:
        NotImplementedError('Guardare ff_evalgroups')


    return precision,recall,TP,FP,FN

def f1_score(prec,recall):
    return 2* prec * recall/(np.finfo(float).eps + prec + recall)

def corr_estimated(g1,gt,t : float):
    """
    True if at least T*|GT| are found
    :param g1: group
    :param gt: ground truth
    :param t: threshold for tolerance
    :return: if the group is correctly estimated
    """
    thr = np.ceil(t * len(gt))
    found = len(set(g1).intersection(set(gt)))
    return  found >= thr

def corr_detected(g1,gt,t:float):
    """
    True if no more than 1 - t*|GT| false subj are in g1
    :param g1: group
    :param gt: ground truth
    :param t: threshold for tolerance
    :return: if the group is correctly identified ( no false element)
    """
    no_det = ((1-t)*len(gt))
    miss = len(set(g1).difference(set(gt)))
    return  miss <= no_det