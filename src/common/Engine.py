# definisce un unico matlab engine
import sys

import matlab
from matlab import engine
import logging

class MatlabEngine:
    _engine = None

    def __init__(self):
        if not MatlabEngine._engine:
            MatlabEngine._engine = engine.start_matlab()

    def __call__(self, *args, **kwargs):
        return MatlabEngine._engine(*args, **kwargs)

    def addpath(self,dirpath):
        """
        Add the ABSOLUTE PATH in the search path for matlab script, this is required as the script needs to be found
        :param dirpath:
        :return:
        """
        MatlabEngine._engine.addpath(dirpath)

    def __getattr__(self, item):
        try:
            return getattr(MatlabEngine._engine, item)
        except matlab.engine.MatlabExecutionError:
            eng = MatlabEngine._engine
            logging.error(f'Attention. Current pwd for matlab eng is {eng.pwd()}. Check the function that you need it\'s visibile from subpath from pwd')
