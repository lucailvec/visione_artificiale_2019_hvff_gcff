import ipywidgets
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.io as sio
from ipywidgets import interact


def import_by_franc(path):
    a = sio.loadmat(path + 'features.mat')
    f = a['features'][0]
    a = sio.loadmat(path + 'groundtruth')  # 'groundtruth_A' or #'groundtruth'

    gt = a['GTgroups'][0]

    ##gt[i] is an array of arrays, each containing objects belonging to a certain class
    ## turn this into a labelling
    ## Some objects are missing in some frames
    ## Reindex these frames
    oldgt = gt
    gt = np.empty_like(oldgt)
    for i in range(gt.shape[0]):
        g = oldgt[i].reshape(-1)
        # Transpose error from one dataset to another
        # size=f[i][:,0].size#(g.size<1) or max([(x.size<1) or x.max() for x in g])
        out = f[i][:, 0].astype(np.int)
        if out.size > 0:
            invert = np.ones(out.max() + 1, dtype=np.int) * -1
            # invert[out]=np.arange(out.size-1,-1,-1)
            invert[out] = np.arange(out.size)
            array = np.arange(out.size)
            ## Labels don't mean anything but label l+1 occurs next 1 making visualisation difficult
            for j in range(g.shape[0]):
                if g[j].size > 0:
                    g2 = g[j].reshape(-1).astype(np.int)
                    # Transpose error from one dataset to another
                    temp = invert[g2]  # array.size-invert[g2]-1
                    array[temp] = temp[0]
            ##compress array
            _, a = np.unique(array, return_inverse=True)
            gt[i] = a

    mask = np.empty(f.shape[0], dtype=np.bool)
    for i in range(f.shape[0]):
        mask[i] = f[i].shape[0] > 0

    f = f[mask]
    f = f[:gt.size]
    gt = gt[mask[:gt.size]]
    return f,gt

def gen_setting(radius,sd_x,sd_y,sd_a,empty,nsamples,quant,s):
    return {'radius': radius,
  'covmat': np.array([[sd_x,0,0],[0,sd_y,0],[0,0,sd_a]]),
  'empty': empty,
  'nsamples': nsamples,
  'quant': quant,
  's': s}

def import_data(dataset_folder):
    features = sio.loadmat(f'{dataset_folder}features.mat')
    features = features['features'][0]
    features = np.concatenate(
        [[np.array([nth, _id, x, y, ang]) for _id, x, y, ang in scene] for nth, scene in enumerate(features)])
    features = features.transpose()
    features = {'frame': features[0].astype('int8'),
                'id': features[1].astype('int8'),
                'x': features[2],
                'y': features[3],
                'angle': features[4]
                }
    features = pd.DataFrame(features)


    groundtruth = sio.loadmat(f'{dataset_folder}groundtruth.mat')
    groundtruth = groundtruth['GTgroups'][0]
    groundtruth = pd.DataFrame([(nth, [z[0] for z in el[0]]) for nth, el in enumerate(groundtruth)],
                               columns=["frame", "groups"])

    settings = sio.loadmat(f'{dataset_folder}settings.mat')['params'][0]
    settings = {'radius': settings[0][0][0][0]/2, #!!!!!!!!!!!!!!! the paper report 30 as "best stride for synthetic" instead of 60, maybe is the diameter?
                'covmat': settings[0][1],
                'empty': settings[0][2][0][0],
                'nsamples': settings[0][3][0][0],
                'quant': settings[0][4][0][0],
                's': settings[0][5][0][0]}
    settings_gc = sio.loadmat(f'{dataset_folder}settings_gc.mat')
    settings_gc = {'stride': settings_gc['stride'][0][0],
                   'mdl': settings_gc['mdl'][0][0]}

    return features,groundtruth,settings,settings_gc

def iter_on_frame(df):
    return ( df[df['frame']==n_frame].drop(['frame'],axis=1).to_numpy() for n_frame in df['frame'].unique() )

def plot_frame(frame: pd.DataFrame, r = 0.5,xlim=None,ylim=None,circles=None,gt=None):
    fig, ax = plt.subplots()
    plt.legend(list(frame['id']))
    if not circles is None:
            [ ax.add_artist(plt.Circle(c,r)) for c,r in circles]
    offset_t = 0.1
    for nth, el in enumerate(frame.itertuples()):
        Index,frame,id,x,y,ang = el
        dx,dy = r*np.cos(ang*180/np.pi) , r*np.sin(ang*180/np.pi)
        rw = plt.arrow(x,y,dx,dy,width=0.1,head_width=10)
        plt.text(x+offset_t,y + offset_t,id,fontsize=12)
        rw.set_color((0,1,0)) # todo rifare colori diversi
    plt.xlim(xlim)
    plt.ylim(ylim)
    if not gt is None:
        g = gt.iloc[0]["groups"]
        plt.text(500,250,"Group: " + "\nGroup: ".join((str(el) for el in g)),fontsize =15)
    plt.show()


def gen_interact_plot(features,xlim,ylim,circles=None,gt=None):
    """
    Plot an interactive plot of feature(x,y,ang) and if present circles ((x,y),r)
    :param features:
    :param gt:
    :param xlim:
    :param ylim:
    :param circles:
    :return:
    """
    @interact
    def plot_arrows_frame(nth_frame = ipywidgets.IntSlider(min=features['frame'].min(),max=features['frame'].max(),step=1,value=0)):
        frame_ex = features[features['frame']==nth_frame]
        plot_frame(frame = frame_ex,r=(xlim[1]-xlim[0])/12,xlim = xlim, ylim = ylim,circles=circles,gt=gt)
    return plot_arrows_frame