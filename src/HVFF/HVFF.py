from typing import Tuple, Union, Optional, List, Any
import numpy as np
import pandas as pd
import scipy.stats as stats
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from src.HVFF.Hough_space import HoughSpace2D

import math


class HVFF:
    def __init__(self, settings: dict, random_state: int = 42, debug: bool = False):
        """
        :rtype: HVFF
        :param debug: enable plot of graph ecc.
        :param settings: Settings have to built of several keys and different values => refer to
        lucailvec/CV-2019-F-formation-HVFF-GCFF settings = {'radius': 60, 'covmat': array([[4.5e+02, 0.0e+00,
        0.0e+00], [0.0e+00, 4.5e+02, 0.0e+00], [0.0e+00, 0.0e+00, 1.0e-03]]), 'empty': 0.5, 'nsamples': 1000,
        'quant': 10, 's': 100}
        """
        self.debug = debug
        self.random_state = random_state
        self.settings: dict = settings

    def _sample(self, values: Union[pd.Series, Tuple[Union[float, int], Union[float, int], float]],
                random_state: Optional[int] = 42) -> Tuple[np.ndarray, np.ndarray]:
        """
        Create self.settings['nsamples'] samples from the mean (a sample) and the others nsamples -1 samples sample
        from gaussian

        :param random_state: int. Specify the random state for the underling sampling
        :param values: x,y,angle or pandas row (series) with name 'x','y' and 'angle'
        :return: Tuple[List[x,y,angle],List[weight or pdf]]
        """
        if isinstance(values, pd.Series):
            mean = values[['x', 'y', 'angle']].to_numpy()
        else:
            mean = values

        multi_norm = stats.multivariate_normal(mean=mean, cov=self.settings['covmat'])
        multi_norm.random_state = random_state
        samples = multi_norm.rvs(size=self.settings['nsamples'] - 1)
        samples = np.append(samples, [mean],
                            axis=0)  # https://docs.scipy.org/doc/numpy/reference/generated/numpy.append.html

        return samples, np.array([multi_norm.pdf(el) for el in samples])

    @staticmethod
    def tseg_to_o_cord(feature: Union[Tuple[float, float, float], pd.Series], stride: float) -> Tuple[float, float]:
        """
        Convert position x,y,angle (x,y,angle) to a candidate o-space at radius/distance equal to stride. The measure
        of angle is degree!!!! :param stride: radius or lenght of the o-space :param feature:  x,y,angle :return: the
        candidate o-space (x,y)
        """
        if isinstance(feature, pd.Series):
            x, y, angle = feature[['x', 'y', 'angle']]
        else:
            x, y, angle = feature

        dx, dy = stride * np.cos(angle * 180 / np.pi), stride * np.sin(angle * 180 / np.pi)
        return x + dx, y + dy

    def identify_o_space(self, subjects: pd.DataFrame, intrusion_thr: float = 0.7):
        """
        #todo manca
        :param intrusion_thr: threshold to record a high probability intrusion/presence
        :param subjects: pd.DataFrame with column named id,x,y,angle
        :return:
        """

        results: List[Tuple[any]] = []
        x_range = (subjects['x'].min() - 2*self.settings['radius'], subjects['x'].max() + 2 * self.settings['radius'])
        y_range = (subjects['y'].min() - 2*self.settings['radius'], subjects['y'].max() + 2 * self.settings['radius'])
        n_quant_xy: Tuple[int, int] = (
            int((x_range[1] - x_range[0]) / self.settings['quant']) \
                , int((y_range[1] - y_range[0]) / self.settings['quant']))

        h_space = HoughSpace2D(x_range, y_range, n_quant_xy)

        samples_by_id : dict = { sub['id']: self._sample(sub, random_state=self.random_state) for index, sub in subjects.iterrows() }

        if self.debug:
            # start self.debug #############################################################
            colors = cm.rainbow(np.linspace(0, 1, subjects['id'].max()))

            plt.figure()
            for k in samples_by_id.keys():  # todo sostituire con list comprehension
                cord_sampled_weight = samples_by_id[k]
                for cord, weight in zip(*cord_sampled_weight):
                    r = 0.7
                    x, y, ang = cord
                    dx, dy = r * np.cos(ang * 180 / np.pi), r * np.sin(ang * 180 / np.pi)
                    rw = plt.arrow(x, y, dx, dy, width=0.05, head_width=2)
                    col_index = int(k) - 1
                    rw.set_color(colors[col_index])

                r = 10
                x, y, ang = subjects[subjects['id']==k].iloc[0][['x', 'y', 'angle']].to_list()
                dx, dy = r * np.cos(ang * 180 / np.pi), r * np.sin(ang * 180 / np.pi)
                rw = plt.arrow(x, y, dx, dy, width=0.1, head_width=4)
                rw.set_color((0, 0, 0))
            plt.xlim(x_range)
            plt.ylim(y_range)
            plt.legend(subjects['id'].to_list())
            plt.title(f'Distribution of vote')
            plt.plot()

                #########################################################################

            plt.figure()
                # start debug #############################################################
            for k in samples_by_id.keys():  # todo sostituire con list comprehension
                cord_sampled_weight = samples_by_id[k]
                xy = [HVFF.tseg_to_o_cord(feature=cord, stride=self.settings['radius']) for cord, weight in zip(*cord_sampled_weight) ]
                x_0,y_0 = [ el[0] for el in xy],[ el[1] for el in xy]
                col_index = int(k) - 1
                plt.plot(x_0, y_0, '.',color=colors[col_index])

                r = 35
                x, y, ang =  subjects[subjects['id']==k].iloc[0][['x', 'y', 'angle']].to_list()
                dx, dy = r * np.cos(ang * 180 / np.pi), r * np.sin(ang * 180 / np.pi)
                rw = plt.arrow(x, y, dx, dy, width=0.1, head_width=4)
                rw.set_color((0,0,0,0.5))#colors[col_index])

            plt.xlim(x_range)
            plt.ylim(y_range)
            plt.legend(subjects['id'].to_list())
            plt.title(f'Distribution of center')
            plt.plot()

            # end debug #############################################################

        for k in samples_by_id.keys():  # todo sostituire con list comprehension o un apply o qualcosa di più leggero
            cord_sampled_weight = samples_by_id[k]

            for cord, weight in zip(*cord_sampled_weight):
                x_ospace, y_ospace = HVFF.tseg_to_o_cord(feature=cord, stride=self.settings['radius'])
                # Step 2: accumulation of votes
                # check if synthetic sample fall in the grid or outside
                if h_space.valid_cordinates(x_ospace, y_ospace):
                    h_space.vote(x=x_ospace, y=y_ospace, id_vote=k, weight=weight)

        while True:

            # Step 2 : \tilde{A}_I(x,y) = #(A_L(x,y))*A_I(x,y)
            card_mat = h_space.card_from_label_space()
            tilde_acc_space = card_mat * h_space.intens_acc_space  # todo ad ogni giro mi basterebbe aggiornare solo un elemento => lascerei fuori questa moltiplic. Tranne quando ne trovo uno valido che devo prunare i pesi e quindi aggiornerei tante posizioni

            # identify the best
            best_flat_index = tilde_acc_space.argmax()
            best_x_idx = int(best_flat_index // h_space.n_quant_xy[1])
            best_y_idx = int(best_flat_index - (best_x_idx * h_space.n_quant_xy[1]))
            best_x_cord, best_y_cord = h_space.index_to_cord(x_index=best_x_idx, y_index=best_y_idx)

            best_weight = h_space.intens_acc_space[best_x_idx][best_y_idx]
            partecipant = h_space.label_acc_space[best_x_idx][best_y_idx]
            non_partecipant = set(subjects['id'].unique()).difference(partecipant)

            if self.debug:
                plt.figure()
                plt.contour(tilde_acc_space)
                plt.show()

            # Phase 3: o-space validation the proposal is invalid iff in the o-space centered at best_x_cord,best_y_cord there are any
            # subject not in the o-space with a weight greather than
            if math.isclose(best_weight, 0.):# il miglior o-space è vuoto e ha intensità ~ zero => ho terminato
                break

            if self.is_o_space_valid(hough_space=h_space, o_space_center=(best_x_cord, best_y_cord),
                                     non_partecipant=non_partecipant,thr_intrusion=intrusion_thr):  # se è valido lo registro
                results.append(((best_x_cord, best_y_cord), best_weight, partecipant))

                # prune of A_I and A_l of partecipant
                for k in set(samples_by_id.keys()).intersection(partecipant):  # todo sostituire con list comprehension
                    cord_sampled_weight = samples_by_id[k]

                    for cord, weight in zip(*cord_sampled_weight):
                        x_ospace, y_ospace = HVFF.tseg_to_o_cord(feature=cord, stride=self.settings['radius'])
                        # Step 2: accumulation of votes

                        if h_space.valid_cordinates(x_ospace, y_ospace):
                            h_space.unvote(x=x_ospace, y=y_ospace, id_vote=k, weight=weight)

            else:  # remove the not valid from tilde_{A}_I
                h_space.intens_acc_space[best_x_idx][best_y_idx] = 0.

        return results

    def is_o_space_valid(self, hough_space: HoughSpace2D, o_space_center: Tuple[float, float],
                         non_partecipant: set,thr_intrusion : float = None) -> bool:
        """
        Return true if in the circle defined as Circle(o_space_center,radius*empty) there are NO external subject (non_partecipant). False if at least one non_partecipant is contained in this area.

        :param thr_intrusion: intrusion threshold to determine if a vote is relevant. If None will be checked only the presence or not the external subject (like implementation)
        :param hough_space: Hough space
        :param o_space_center: x and y REAL WORLD cordinates
        :param non_partecipant: list of id not in the
        :return: true if it is valid or false
        """
        x_o, y_o = o_space_center
        radius = self.settings['radius']
        empty = self.settings['empty']
        step_resolution = self.settings[
                              'quant'] / 2  # todo find a good method to iterate on the cells of the inner o-space

        for x_index, y_index in (hough_space.cord_to_index(_x, _y) for _x in
                                 np.arange(x_o - radius * empty, x_o + radius * empty, step_resolution) for _y in
                                 np.arange(y_o - radius * empty, y_o + radius * empty, step_resolution) if
                                 (x_o - _x) ** 2 + (y_o - _y) ** 2 < (
                                         radius * empty) ** 2 and hough_space.valid_cordinates(_x, _y)):
            try:
                if thr_intrusion is None:
                    if len(non_partecipant.intersection(hough_space.label_acc_space[x_index][y_index]))>=1:
                        return False
                else:
                    if any(( hough_space.intens_by_id[x_index][y_index][id] >= thr_intrusion for id in non_partecipant )): #if present is the cumulative sum in this area, otherwise 0 (and predicate false)
                        return False
            except IndexError:
                return False

        return True
