from collections import defaultdict
from collections.__init__ import defaultdict
from typing import Tuple, Optional, Sequence, List, Any
from itertools import chain

import numpy as np



class HoughSpace2D:
    """
    It define 2 main properties: Intensity accumulation space and Label accumulation space. The label accumulation space work by hashing the id, so it needs to be an hashable type.

    """

    def __init__(self, range_x: Tuple[float, float] = None, range_y: Tuple[float, float] = None,
                 n_quant_xy: Tuple[int, int] = None):
        """
        :param range_x: (min,max) for the first axes
        :param range_y: (min,max) for the second axes
        :param n_quant_xy: (n_split_x,n_split_y) number of bucket for the
                range from max to min for a total of (max-min)/split
        """
        assert all([isinstance(el, float) for el in chain(range_x, range_y)]), 'ranges need to be Tuple(float,float)'
        assert isinstance(n_quant_xy[0], int) and isinstance(n_quant_xy[1], int), 'n_quant needs to be tuple of integer'
        assert range_x[1] > range_x[0], 'Tuple need to be in the form (min,max)'
        assert range_y[1] > range_y[0], 'Tuple need to be in the form (min,max)'
        assert n_quant_xy[0] > 0, 'Quant, cannot be negative or zero'
        self._x_lim: Tuple[float, float] = range_x
        self._y_lim: Tuple[float, float] = range_y
        self.n_quant_xy: Tuple[int, int] = n_quant_xy
        self._quant_x: float = (range_x[1] - range_x[0]) / n_quant_xy[0]
        self._quant_y: float = (range_y[1] - range_y[0]) / n_quant_xy[1]

        # intensitiy accumulation space
        self.intens_acc_space: np.ndarray[float] = np.zeros(self.n_quant_xy, dtype=float)
        # label accumulation space
        self.label_acc_space: List[List[set]] =  [[set() for el in range(self.n_quant_xy[1])] for el in range(self.n_quant_xy[0])]

        # intensity accumulation by id
        self.intens_by_id : np.ndarray[dict] =   [[ defaultdict(lambda : 0.) for el in range(self.n_quant_xy[1])] for el in range(self.n_quant_xy[0])] #todo sicuramente si può rimuovere o sostituire con un array finito

    def vote(self, x, y, id_vote, weight: Optional[float] = None, intrusion_thr : float =0.7):
        """
        Acquire a vote from id_vote to the position discretized (x,y) with a weight of weight.

        :param intrusion_thr: for the element those are NOT in the center of the o-space(acc cell), the o-space may be invalid if the max of likelihood in an external subject of the o-space is seen in a sub-o-space of radius empty*radius.
        :param x: x position
        :param y: y position
        :param id_vote: who vote
        :param weight: Optional. If it let to be None +1 will be add to
                intens_acc_space[x_index,y_index], weight if set.
        :return:
        """

        x_index, y_index = self.cord_to_index(x,y)

        #if weight >= intrusion_thr: # write the vote iff it is over the thr
        #    self.external_space[x_index][y_index].add(id_vote)
        self.intens_by_id[x_index][y_index][id_vote] = max(  1 if weight is None else weight,self.intens_by_id[x_index][y_index][id_vote])

        self.intens_acc_space[x_index][y_index] += 1 if weight is None else weight
        self.label_acc_space[x_index][y_index].add(id_vote)

    def valid_cordinates(self,x,y):
        x_min, x_max = self._x_lim
        y_min, y_max = self._y_lim
        if (y > y_max or y < y_min) or ( x > x_max or x < x_min):
            return False
        else:
            return True

    def unvote(self, x, y, id_vote, weight: Optional[float] = None):
        """

        Remove vote (weight and label) in the current space

        :param x:
        :param y:
        :param id_vote:
        :param weight:
        :return:
        """

        x_index, y_index = self.cord_to_index(x, y)

        self.intens_by_id[x_index][y_index][id_vote] = min( 1 if weight is None else weight, self.intens_by_id[x_index][y_index][id_vote])
        self.intens_acc_space[x_index][y_index] -= 1 if weight is None else weight
        self.label_acc_space[x_index][y_index] = self.label_acc_space[x_index][y_index].difference({id_vote})

    def index_to_cord(self, x_index: int, y_index: int) -> Tuple[float, float]:
        """

        Convert a grid position in a cordinate in real world based on HoughSpace init

        :param x_index: x grid cordinate
        :param y_index: y grid cordinate
        :return: tuple(x_world,y_world) cordinate
        """
        assert isinstance(x_index,int) and isinstance(y_index,int), 'x and y have to be integer'
        assert 0 <= x_index < self.n_quant_xy[0], f'x_index: {x_index} outOfBound. Needs to be between 0-({self.n_quant_xy[0]}-1)'
        assert 0 <= y_index < self.n_quant_xy[1], f'y_index: {y_index} outOfBound. Needs to be between 0-({self.n_quant_xy[1]}-1)'
        # todo centro nell'area del campionamento, DA CONTROLLARE
        _x = self._x_lim[0] + x_index * self._quant_x + self._quant_x * 0.5
        _y = self._y_lim[0] + y_index * self._quant_y + self._quant_y * 0.5
        return _x, _y

    def cord_to_index(self, x_cord: float, y_cord: float) -> Tuple[int,int]:
        """

        Convert a position in real world cordinates to a grid index

        :param x_cord:
        :param y_cord:
        :return:
        """
        x_min, x_max = self._x_lim
        y_min, y_max = self._y_lim
        if x_cord > x_max or x_cord < x_min:
            raise ValueError(f'x actual: {x_cord} instead in range ({x_min},{x_max})')
        if y_cord > y_max or y_cord < y_min:
            raise ValueError(f'y actual: {y_cord} instead in range ({y_min},{y_max})')

        # divisione intera!
        x_index, y_index = (x_cord - x_min) // self._quant_x, (y_cord - y_min) // self._quant_y

        return int(x_index), int(y_index)

    def card_from_label_space(self) -> np.ndarray:
        """
        Convert the label accumulation space to a matrix of the same size but every list of id converted into the cardinality of the list, for each position.
        :return: Mat represent the cardinality for each vote
        """
        v_f = np.vectorize(lambda x : len(x))
        d = v_f(self.label_acc_space).reshape(self.n_quant_xy[0], self.n_quant_xy[1])
        return d
