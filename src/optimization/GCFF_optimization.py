from itertools import product
import sys
sys.path.append("..")
import numpy as np
from common.import_data import iter_on_frame, import_data
from joblib import Parallel, delayed
from sklearn.model_selection import StratifiedShuffleSplit

from src.GCFF.GCFF import gc
from src.common.metrics import evalgroup, f1_score

path = '../../data/Synth/'
features, groundtruth, settings, settings_gc = import_data(path)

stride, mdl = settings_gc['stride'], settings_gc['mdl']

n_values = 3
mult_range = 1.4
grid = { 'mdl' :  [mdl-200,mdl,mdl+100],#np.linspace(mdl/5,mdl*5,num=n_values,dtype=int),
         'stride' : [int(stride*0.9),stride,int(stride*1.1)]#np.linspace(stride/5,stride*5,num=n_values,dtype=int)
}

n_splits = 3
random_state = 42

frames = features['frame'].unique()
X = frames

group_frame = frames // 10
y = group_frame

sss_outer = StratifiedShuffleSplit(n_splits=n_splits, test_size=0.3,
                                   random_state=random_state)  # sss_inner = StratifiedShuffleSplit(n_splits=n_splits,test_size=0.3,random_state=random_state)




results = np.empty((n_splits,3),dtype=object)

for nth, (val_index, test_index) in enumerate(sss_outer.split(X, y)):
    inner_frame = X[val_index]
    outer_frame = X[test_index]
    inner_iter_frame = list(iter_on_frame(features[features['frame'].isin(inner_frame)]))
    outer_iter_frame = list(iter_on_frame(features[features['frame'].isin(outer_frame)]))

    inner_iter_gt = [g for g in groundtruth['groups'][val_index].to_numpy()]
    outer_iter_gt = [g for g in groundtruth['groups'][val_index].to_numpy()]

    def parse_frame(frame,gt, m, s):  # ,features,groundtruth):
        res = gc(frame, mdl=int(m), stride=int(s))
        prec, rec, tp, fp, fn = evalgroup(groups=res, gt=gt)
        return f1_score(prec, rec)

    score = []
    for nth_param, others in enumerate(product(*grid.values())):
        _mdl, _stride = others
        #score_inner = Parallel(n_jobs=1, verbose=5)(delayed(parse_frame)(f,g,_mdl,_stride) for f,g in zip(inner_iter_frame,inner_iter_gt))
        score_inner = [ parse_frame(f,g,_mdl,_stride) for f,g in zip(inner_iter_frame,inner_iter_gt)]
        score.append(np.mean(score_inner))

    # best param
    index = np.argmax(score)
    best_settings = next(x for i, x in enumerate(product(*grid.values())) if i == index)
    best_score = score[index]

    print(f'{nth}° fold, score: {best_score} params: {best_settings}')
    results[nth,:]= [index, best_score, best_settings]

print(f'Unbiased f1-score: {results[:,1].mean()}')
