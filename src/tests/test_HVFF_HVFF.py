import unittest

from src.HVFF.HVFF import HVFF
from src.common.import_data import import_data
from src.common.metrics import evalgroup


class Test_HVFF(unittest.TestCase):

    def test_poor_choice_of_param(self):
        dataset_folder = "../../data/"
        example_name = "Synth/"
        features, groundtruth, settings, settings_gc = import_data(dataset_folder+example_name)
        settings['covmat']=settings['covmat']/2

        nth = 0
        frame_nth = features[features['frame'] == nth]
        gt = groundtruth[groundtruth['frame'] == nth].iloc[0]
        gt_group = gt['groups']
        hvff = HVFF(settings=settings, random_state=42,debug=False)

        results  = hvff.identify_o_space(subjects=frame_nth, intrusion_thr=0.0007)
        centers, weights, partecipants = zip(*results)
        part_2_or_more = [p for p in partecipants if len(p)>=2]

        prec,rec,tp,fp,fn = evalgroup(part_2_or_more, gt=gt_group, th=2/3)


        self.assertEqual(rec,0.5)
        self.assertEqual(prec,0.5)

