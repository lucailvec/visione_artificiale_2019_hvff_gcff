# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 17:16:31 2013

@author: chrisr
"""
import sys
sys.path.append("..")
import logging
from itertools import product
from math import inf
from joblib import Parallel, delayed
from sklearn.model_selection import StratifiedShuffleSplit
from src.common.metrics import evalgroup, f1_score
import matlab
import numpy as np
from common.Engine import MatlabEngine

eng = MatlabEngine()
eng.addpath('/home/lucavecchi/uni/visione_artificiale/src/GCFF/')


def find_locs(f, stride=35):
    "Estimate focal centers for each person given features"
    locs = np.empty((f.shape[0], 2))
    locs[:, 0] = f[:, 1] + np.cos(f[:, 3]) * stride
    locs[:, 1] = f[:, 2] + np.sin(f[:, 3]) * stride
    return locs


def calc_distance(loc, f, labels, mdl):  # loc e labels hanno la stessa lunghezza
    """Given focal localtions, raw locations(f) and initial labelling l find
    cost of assigning  people to new locations given by the mean of their
    labelling"""
    u = np.unique(labels)
    dist = np.empty((loc.shape[0], u.shape[0]))  # Array_{fform, subject}
    for i in range(u.shape[0]):  # per ogni subject ? i subject possono essere meno o più delle labels?
        means = loc[labels == i, :].mean(0)  # faccio la media devi vettori (x,y) in cui l'iesimo soggetto
        # è nel labels ? Un subject è dentro a labels più volte con il risultato che ha più f form?
        dist[:, i] = ((loc - means) ** 2).sum(1)  # calcolo la norm2 tra tutti gli f-formation e la media in cui i c'è
        # computed sum-squares distance, now

        mask = np.arange(loc.shape[0])
        mask = mask[dist[:, i] < mdl]  # tutte le loc in cui ho valore < mdl (più vicino)
        disp = f[:, 1:3].copy()  # copio la posizione di tutte le altre persone (x,y)
        disp -= means  # ci sottraggo - la media ?!?!? Guardo le spalle? in direzione opposta?
        for j in mask:
            for k in mask:
                distk = np.linalg.norm(disp[k])  # la distanza (da 0) di (subj_k - mean_i) (centroide di i)
                distj = np.linalg.norm(disp[j])  # la distanza (da 0) di (subj_j - mean_i)
                if distk > distj:  #
                    inner = disp[k].dot(disp[j])  # norm(A)norm(B)cos(A & B)
                    norm = distk * distj  # norm(A)norm(B)
                    if inner / norm > .75:  # cos(A & B) > 0.75
                        dist[k, i] += 100 ** (inner / norm * distk / distj)  # 100^(cos(A & B)*(norm(A)/norm(B)))
    return dist


def init(locs, f, mdl):
    return calc_distance(locs, f, np.arange(locs.shape[0]), mdl)


def gc(f, stride=35, mdl=3500, starts_from_0=False):
    """
    :param f: 2d array. rows are different subject and cols are (id,x_pos,y_pos,angle)
    :param stride: radius of o-space
    :param mdl: minimum descriptor length
    :return: spurious and correct f-formation
    """
    assert isinstance(stride,int)
    assert isinstance(mdl,int)

    locs = find_locs(f, stride)  # locs array x,y degli f-formation da (List[x,y,alpha])
    unary = init(locs, f, mdl)  #
    blank = np.zeros(f.shape[0], dtype=np.double)  # sia shape,1 - shape,0 che solo shape
    neigh = blank.copy()
    weight = blank.copy()
    seg = np.arange(f.shape[0], dtype=np.double)# sono gli assegnamenti dell'i-esimmo soggetto al seg[i]-esima f-formation
    for i in range(1000):
        segold = seg.copy()
        mdl_arr = np.ones((unary.shape[1], 1)) * mdl
        # Run Graph-cuts
        other, seg = eng.expand(matlab.double(unary.tolist()),
                                matlab.double(neigh.tolist()),
                                matlab.double(weight.tolist()),
                                seg.tolist(),
                                matlab.double(mdl_arr.tolist()),
                                float(inf),#np.inf,
                                nargout=2
                                )
        # discard unused labells

        seg = np.array(seg).astype(np.int)
        _, seg = np.unique(seg, return_inverse=True)
        # refit distances
        unary = calc_distance(locs, f, seg, mdl)
        if all(seg == segold):
            break
    return array_to_group(seg,np.unique(f[:,0]))


def array_to_group(arr, label):
    return [label[id_group == arr].astype(int) for id_group in np.unique(arr) if sum(id_group == arr) >= 2]
