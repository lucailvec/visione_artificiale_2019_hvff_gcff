% Copyright 2014 Francesco Setti and Marco Cristani
% Department of Computer Science
% University of Verona
% http://www.di.univr.it/
% 
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, subject to the
% following conditions: 
%  * The above copyright notice and this permission notice shall be
%    included in all copies or substantial portions of the Software. 
%  * The Software is provided "as is", without warranty of any kind.
%
% September 2014
%

function [ acc_out, acc_lbl ] = ff_buildaccum( features, params, accmode, votegrid, VERBOSE )
% This function computes the accumulation voting space.
% 
% This is a non-optimized version which computes everything and then
% assigns correct outputs.

if ~exist('VERBOSE','var')
    VERBOSE = false ;
end

% Convert grid matrix into an array of points
grid_pos = reshape(votegrid,[],2) ;

% Define the input features as o_features (original). (graphic purposes)
if VERBOSE
    o_features = features ;
    o_features_acc = o_features;
    o_features_acc(:,2:3) = (o_features(:,2:3)-repmat(min(grid_pos),size(o_features,1),1)) / params.quant;
end

% Define the number of individuals in the scene and their labels
num_subj = size(features,1) ;
list_subj = features(:,1) ;

% Check the input features: if it is an empty matrix, return an empty
% accumulation space.
if isempty(features)
    acc_out = [] ;
    acc_lbl = [] ;
    return ;
end


%% SAMPLING THE O-CENTERS
% Sample from each individual a set of o-space center candidates.

% For each individual, we sample a set of candidates varying the position
% and orientation
newfeatures = [] ;
for idxSubj = 1:num_subj
    % Pick the i-th individual and add a column with the confidence score
    % (pdf of itself according to covariance matrix).
    seed = [features(idxSubj,:), mvnpdf(features(idxSubj,2:4),features(idxSubj,2:4),params.covmat)] ;
    % Sample the feature 'seed' and add the confidence score
    additional(:,1)   = ones(params.nsamples,1) .* seed(1) ;
    additional(:,2:4) = mvnrnd( seed(2:4)', params.covmat, params.nsamples) ;
    additional(:,5)   = mvnpdf( additional(:,2:4), seed(2:4), params.covmat) ;
    % Add the new samples to the existing features
    newfeatures = [ newfeatures; seed; additional] ;
end

features = newfeatures ;
num_data = size(features,1) ;
% NOTE: we overwrite the variable features. From now features is not the
% original features as input, but the sampled features. Note that these are
% not the groups center candidates yet!

% Convert each sample to the center of the related o-space.
p_features = features(:,[1:3,5]) ;
p_features(:,2:3) = features(:,2:3) - params.radius * [cos(features(:,4)+pi),sin(features(:,4)+pi)] ;

if VERBOSE
    figure(1000),
    subplot(1,2,1), cla
    scatter(p_features(:,2),p_features(:,3),0.1,'g');
    hold on,
    ff_plot_person_tv(o_features,'b',70);
    axis equal
    axis([min(grid_pos(:,1)),max(grid_pos(:,1)),min(grid_pos(:,2)),max(grid_pos(:,2))])
    axis ij
end


%% QUANTIZATION
% Fit the o-centers candidates into the grid

% Find nearest neighbour in grid_pos for each o-center candidate
idx = knnsearch( grid_pos, p_features(:,2:3)) ;

% Redefine the o-center candidates
q_features = p_features ;
q_features(:,2:3) = grid_pos(idx,:) ;
% NOTE: this variable is not used in the computation of accumulators but
% could be useful for plotting.


%% BUILD ACCUMULATORS
% Build the accumulators

% Initialize accumulators:
acc_int    =  cell(size(votegrid,1),size(votegrid,2)) ;    % intensities
acc_label  =  cell(size(votegrid,1),size(votegrid,2)) ;    % ID labels
acc_prob   =  zeros(size(votegrid,1),size(votegrid,2)) ;   % probability
% acc_id     =  zeros(size(grid,1),size(grid,2)) ;
acc_lin    =  zeros(size(votegrid,1),size(votegrid,2)) ;   % linear
acc_num    =  zeros(size(votegrid,1),size(votegrid,2)) ;   % number of individuals
acc_ent    =  zeros(size(votegrid,1),size(votegrid,2)) ;   % entropy

% For each o-center, populate the accumulators
[rr,cc] = ind2sub([size(votegrid,1),size(votegrid,2)],idx) ;
for idxFeat = 1:num_data
    
    acc_int{rr(idxFeat),cc(idxFeat)}   = [ acc_int{rr(idxFeat),cc(idxFeat)}, features(idxFeat,5)] ;
    acc_label{rr(idxFeat),cc(idxFeat)} = [ acc_label{rr(idxFeat),cc(idxFeat)}, features(idxFeat,1)] ;
    acc_prob(rr(idxFeat),cc(idxFeat))  = acc_prob(rr(idxFeat),cc(idxFeat)) + features(idxFeat,5);
    % acc_id(rr(idxFeat),cc(idxFeat))    = features(idxFeat,1);
    
end

[rro,cco] = find(acc_prob~=0) ;
for idxGrid = 1:length(rro)
    acc_num(rro(idxGrid),cco(idxGrid)) = length(unique(acc_label{rro(idxGrid),cco(idxGrid)})) ;
    acc_ent(rro(idxGrid),cco(idxGrid)) = acc_num(rro(idxGrid),cco(idxGrid)) * ff_wentropy(acc_label{rro(idxGrid),cco(idxGrid)},acc_int{rro(idxGrid),cco(idxGrid)},6) ;
%     acc_ent(rro(idxGrid),cco(idxGrid)) = wentropy(acc_label{rro(idxGrid),cco(idxGrid)},acc_int{rro(idxGrid),cco(idxGrid)},6) ;
    acc_lin(rro(idxGrid),cco(idxGrid)) = acc_num(rro(idxGrid),cco(idxGrid)) * acc_prob(rro(idxGrid),cco(idxGrid)) ;
end

% Define a new accumulator which is simply acc_label in a matrix
% configuration instead of cell-array configuration.
acc_label_mat = zeros(size(votegrid,1),size(votegrid,2),num_subj) ;
[aa,bb] = find(acc_num~=0) ;
for ii = 1:length(aa)
    [~,~,ss] = intersect(unique(acc_label{aa(ii),bb(ii)}),list_subj) ;
    acc_label_mat( aa(ii), bb(ii), ss) = 1 ;
end



%% SET OUTPUTS

switch accmode
    case 'linear'
        acc_out = acc_lin ;
    case 'bwent'
        acc_out = acc_ent ;
end
acc_lbl = acc_label_mat ;


if VERBOSE
    figure(1000),
    subplot(1,2,2), cla
    imagesc(acc_out),
    hold on,
    ff_plot_person_tv(o_features_acc,'w',70/params.quant);
    axis image
end

