%% example_ICIP13.m
% This script is just an example of how to use the HVFF code to run
% experiments with the ICIP version.


%% INITIALIZATION

% Clean the workspace
clear all,
close all,
clc,

% Set Verbose mode
VERBOSE = false ;

% Set parallel computing mode
PAR_MODE = false ;

% Set the accumulation mode
ACC_MODE = 'bwent' ;


% Set data folder
seqpath = './data/Synth' ;
% NB: edit here your own path to data!!!


% Load features
load( fullfile(seqpath,'features.mat') ) ;

% Load settings parameters
load( fullfile(seqpath,'settings.mat') ) ;


% Set evaluation mode (if 1, compare with ground truth)
GTmode = true ;

% Load groundtruth
if GTmode
    load( fullfile(seqpath,'groundtruth.mat') ) ;
    for idxFrame = 1:length(GTtimestamp)
        GTgroups{idxFrame} = ff_deletesingletons(GTgroups{idxFrame}) ;
    end
end

% Trick to make the processing faster: process only the frames with ground
% truth.
if GTmode
    % Find the index of frames for which we have GT
    [~,indFeat] = intersect(timestamp,GTtimestamp) ;
    timestamp = timestamp(indFeat) ;
    features  = features(indFeat) ;
end
% NOTE: comment this lines if you are using some sort of temporal
% constraint.


% NOTE: it is NOT possible to use parallel computation in this version!!!
% % % % Open parallel sessions (optional). If you comment this lines, please
% % % % replace 'parfor' with 'for' at line 92.
% % % if PAR_MODE && matlabpool('size')==0
% % %     matlabpool(6)
% % %     VERBOSE = false ;
% % % end
% % % % NOTE: set the number of sessions according with your computer's
% % % % architecture.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  DEBUG SECTION
%%%  Quick tests of different parameters
%%%  PLEASE AVOID USING THIS LINES!!! USE IT ONLY FOR QUICK TESTS!!!
% params.quant = 1 ;
% params.covmat = params.covmat * 5 ;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% PROCESSING

% Initialize evaluation variables
TP = zeros(1,length(timestamp)) ;
FP = zeros(1,length(timestamp)) ;
FN = zeros(1,length(timestamp)) ;
precision = zeros(1,length(timestamp)) ;
recall = zeros(1,length(timestamp)) ;

% Generate voting grid.
[ votegrid, votegrid_pos ] = ff_gengrid( features, params, params.quant ) ;

tic

for idxFrame = 1:length(timestamp)
    
    if ~PAR_MODE
        fprintf('Frame: %d/%d\n', idxFrame, length(timestamp))
    end
    
    % Initialize detection outputs
    group{idxFrame} = [] ;
    score{idxFrame} = [] ;
    center{idxFrame} = [] ;
    
    count = 1 ;
    clear all*
    for idxCard = 1:3
        
        % Generate temp params
        tmp_params = params ;
        tmp_params.radius = params.s / (2*sin(pi/(2*idxCard))) ;
        tmp_params.card = 2*idxCard:(2*idxCard+1) ;
        tmp_params.covmat = params.covmat * idxCard ;
        
        % Build accumulator
        [accval,acclbl] = ff_buildaccum( features{idxFrame}, tmp_params, ACC_MODE, votegrid, VERBOSE ) ;
        accnum = sum(acclbl,3) ;
        accnum(accnum<(2*idxCard)|accnum>(2*idxCard+1)) = 0 ;
        accnum(accnum>0) = 1 ;
        accval = accval .* accnum ;
        acclbl = acclbl .* repmat(accnum,[1,1,size(acclbl,3)]) ;
        
        % Extract local maxima from the accumulator
        [groupTot{idxFrame}{idxCard}, scoreTot{idxFrame}{idxCard}, centerTot{idxFrame}{idxCard}] = ff_greedyopt( accval, acclbl, ...
            features{idxFrame}, votegrid_pos, tmp_params) ;
        
        for jj = 1:length(groupTot{idxFrame}{idxCard})
            allGroups{count} = groupTot{idxFrame}{idxCard}{jj} ;
            allCenters{count} = centerTot{idxFrame}{idxCard}(jj) ;
            allScores_or{count} = scoreTot{idxFrame}{idxCard}(jj) ;
            allScores_norm{count} = scoreTot{idxFrame}{idxCard}(jj) / numel(groupTot{idxFrame}{idxCard}{jj}) ;
            count = count + 1 ;
        end
        
    end
    
    if count > 1
        [allScores,sortIdx ] = sort(cell2mat(allScores_norm),'descend') ;
        allGroups = allGroups(sortIdx) ;
        allCenters = allCenters(sortIdx) ;
        countGroups = 1 ;
        others = features{idxFrame}(:,1) ;
        for idxGroup = 1:length(allGroups)
            [c, ia, ib] = intersect(allGroups{idxGroup},others) ;
            if length(c)==length(allGroups{idxGroup})
                group{idxFrame}{countGroups} = c ;
                others(ib) = [] ;
                countGroups = countGroups + 1 ;
            end
        end
    else
        group{idxFrame} = [] ;
    end
    
    if ~PAR_MODE
        fprintf('   FOUND:-- ')
        if ~isempty(group{idxFrame})
            for ii=1:size(group{idxFrame},2)
                fprintf(' %i',group{idxFrame}{ii})
                fprintf(' |')
            end
        else
            fprintf(' No Groups ')
        end
        fprintf('\n')
    end
    
    if GTmode && ~isempty(intersect(timestamp,GTtimestamp))
        [tt,ia,ib] = intersect(GTtimestamp,timestamp(idxFrame)) ;
        if ~PAR_MODE
            fprintf('   GT   :-- ')
            if ~isempty(GTgroups{ia})
                for ii=1: size(GTgroups{ia},2)
                    fprintf(' %i',GTgroups{ia}{ii})
                    fprintf(' |')
                end
            else
                fprintf(' No Groups ')
            end
            fprintf('\n');
        end
        
        [precision(idxFrame),recall(idxFrame),TP(idxFrame),FP(idxFrame),FN(idxFrame)] = ff_evalgroups(group{idxFrame},GTgroups{ia},'card') ;
        
        if ~PAR_MODE
            fprintf('   Frame %i --> TP=%i | FP=%i | FN=%i | prec=%d | rec=%d\n',idxFrame,TP(idxFrame),FP(idxFrame),FN(idxFrame),precision(idxFrame),recall(idxFrame));
            fprintf('--------------\n')
        end
        
    end
    
end


% Compute the average performances over the entire sequence
if GTmode
    [~,indFeat] = intersect(timestamp,GTtimestamp) ;
    pr_avg = mean(precision(indFeat)) ;
    re_avg = mean(recall(indFeat)) ;
    F1_avg = 2 * pr_avg * re_avg / ( pr_avg + re_avg ) ;
    fprintf('Average Precision: -- %d\n',pr_avg)
    fprintf('Average Recall: -- %d\n',re_avg)
    fprintf('Average F1 score: -- %d\n',F1_avg)
end

toc


% Automatic closure of multiple sessions
if matlabpool('size')>0
    matlabpool close
end


% Save results
save( fullfile(seqpath,strcat('res_ICIP.mat')), 'group' ) ;

