% Copyright 2014 Francesco Setti and Marco Cristani
% Department of Computer Science
% University of Verona
% http://www.di.univr.it/
% 
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, subject to the
% following conditions: 
%  * The above copyright notice and this permission notice shall be
%    included in all copies or substantial portions of the Software. 
%  * The Software is provided "as is", without warranty of any kind.
%
% September 2014
%

function E = wentropy(data,weights,nbin)

  

% calculate histogram counts
[p,C] = hist(data(:),nbin);
C(p==0) = [];
p(p==0) = [];

W = cell(length(C),1);
for i=1:length(data(:))
    classe(i) = knnclassify(data(i),C',[1:length(C)]',1);
    W{classe(i)}=[W{classe(i)}, weights(i)];
end

% remove zero entries in p 


for i=1:length(C)
    class_weights(i)=sum(W{classe(i)});
end

% normalize p so that sum(p) is one.
p = p ./ sum(p(:));
class_weights = class_weights./sum(class_weights(:));

E = -sum(class_weights.*p.*log2(p));
  
