HOUGH VOTING FOR F-FORMATION detection

This folder contains a summary of the code used for the experiments for 3 different conference papers, aiming to detect groups of people (F-formations) based on Hough-Voting strategy:

1) M. Cristani, L. Bazzani, G. Paggetti, A. Fossati, D. Tosato, A. Del Bue, G. Menegaz, V. Murino: "Social interaction discovery by statistical analysis of F-formations", BMVC 2011 (10.5244/C.25.23)

2) F. Setti, H. Hung, M. Cristani: "Group detection in still images by F-formation modeling: A comparative study", WIAMIS 2013 (10.1109/WIAMIS.2013.6616147)

3) F. Setti, O. Lanz, R. Ferrario, V. Murino, M. Cristani: "Multi-Scale F-Formation Discovery for Group Detection", ICIP 2013
 

CODE:
=====
Please refer to the example_XXX.m file for a detailed example of how to use the code. Main functions are in the main folder named ff_*.m, while some utilities (perhaps from other sources) are in 'utility' folder.


DATASETS:
=========
The datasets used for the mentioned papers are provided in a separate folder at the same URL. For more details about the datasets, please refer to the mentioned papers.
Each dataset folder contains 3 files:
- 'features.mat' -> all the detections are stored in the variable 'features'; features{t} is an Nx4 matrix that stores all the detections at frame 't', columns are [ ID, x, y, orientation ]
- 'groundtruth.mat' -> groups ground truths, timestamp variable is needed to match with the features.mat file.
- 'settings.mat' -> parameter settings for the experiments (see the code for more details). This file is just an example and it could be replicated for different versions of experiments. Please use your own settings file if you want to run other experiments.


REEFERENCES:
============
If you use this code for your experiments and you publish the results, please cite the above mentioned papers, according with the experiments you conduct.


IMPORTANT NOTE:
===============
For any problem by using this code or with the datasets, please email franzsetti@gmail.com or marco.cristani@univr.it


