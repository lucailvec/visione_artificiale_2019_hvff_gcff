%% example_BMVC11.m
% This script is just an example of how to use the HVFF code to run
% experiments with the BMVC version.


%% INITIALIZATION

% Clean the workspace
clear all,
close all,
clc,

% Set Verbose mode
VERBOSE = true ;

% Set parallel computing mode
PAR_MODE = true ;

% Set the accumulation mode
ACC_MODE = 'linear' ;


% Set data folder
seqpath = '../data/Synth' ;
% NB: edit here your own path to data!!!


% Load features
load( fullfile(seqpath,'features.mat') ) ;

% Load settings parameters
load( fullfile(seqpath,'settings.mat') ) ;


% Set evaluation mode (if 1, compare with ground truth)
GTmode = true ;

% Load groundtruth
if GTmode
    load( fullfile(seqpath,'groundtruth.mat') ) ;
    for idxFrame = 1:length(GTtimestamp)
        GTgroups{idxFrame} = ff_deletesingletons(GTgroups{idxFrame}) ;
    end
end

% Trick to make the processing faster: process only the frames with ground
% truth.
if GTmode
    % Find the index of frames for which we have GT
    [~,indFeat] = intersect(timestamp,GTtimestamp) ;
    timestamp = timestamp(indFeat) ;
    features  = features(indFeat) ;
end
% NOTE: comment this lines if you are using some sort of temporal
% constraint.


% Open parallel sessions (optional). If you comment this lines, please
% replace 'parfor' with 'for' at line 92.
if PAR_MODE && matlabpool('size')==0
    matlabpool(6)
    VERBOSE = false ;
end
% NOTE: set the number of sessions according with your computer's
% architecture.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  DEBUG SECTION
%%%  Quick tests of different parameters
%%%  PLEASE AVOID USING THIS LINES!!! USE IT ONLY FOR QUICK TESTS!!!
% params.quant = 1 ;
% params.covmat = params.covmat * 5 ;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% PROCESSING

% Initialize evaluation variables
TP = zeros(1,length(timestamp)) ;
FP = zeros(1,length(timestamp)) ;
FN = zeros(1,length(timestamp)) ;
precision = zeros(1,length(timestamp)) ;
recall = zeros(1,length(timestamp)) ;

% Generate voting grid.
[ votegrid, votegrid_pos ] = ff_gengrid( features, params, params.quant ) ;

tic

parfor idxFrame = 1:length(timestamp)
    
    if ~PAR_MODE
        fprintf('Frame: %d/%d\n', idxFrame, length(timestamp))
    end
    
    % Initialize detection outputs
    group{idxFrame} = [] ;
    score{idxFrame} = [] ;
    center{idxFrame} = [] ;
    
    % Build accumulator
    [accval,acclbl] = ff_buildaccum( features{idxFrame}, params, ACC_MODE, votegrid, VERBOSE ) ;
    
    % Extract local maxima from the accumulator
    [group{idxFrame}, ~, center{idxFrame}] = ff_greedyopt( accval, acclbl, ...
        features{idxFrame}, votegrid_pos, params) ;
    group{idxFrame} = ff_deletesingletons(group{idxFrame}) ;
    
    if ~PAR_MODE
        fprintf('   FOUND:-- ')
        if ~isempty(group{idxFrame})
            for ii=1:size(group{idxFrame},2)
                fprintf(' %i',group{idxFrame}{ii})
                fprintf(' |')
            end
        else
            fprintf(' No Groups ')
        end
        fprintf('\n')
    end
    
    if GTmode && ~isempty(intersect(timestamp,GTtimestamp))
        [tt,ia,ib] = intersect(GTtimestamp,timestamp(idxFrame)) ;
        if ~PAR_MODE
            fprintf('   GT   :-- ')
            if ~isempty(GTgroups{ia})
                for ii=1: size(GTgroups{ia},2)
                    fprintf(' %i',GTgroups{ia}{ii})
                    fprintf(' |')
                end
            else
                fprintf(' No Groups ')
            end
            fprintf('\n');
        end
        
        [precision(idxFrame),recall(idxFrame),TP(idxFrame),FP(idxFrame),FN(idxFrame)] = ff_evalgroups(group{idxFrame},GTgroups{ia},'card') ;
        
        if ~PAR_MODE
            fprintf('   Frame %i --> TP=%i | FP=%i | FN=%i | prec=%d | rec=%d\n',idxFrame,TP(idxFrame),FP(idxFrame),FN(idxFrame),precision(idxFrame),recall(idxFrame));
            fprintf('--------------\n')
        end
        
    end
    
end


% Compute the average performances over the entire sequence
if GTmode
    [~,indFeat] = intersect(timestamp,GTtimestamp) ;
    pr_avg = mean(precision(indFeat)) ;
    re_avg = mean(recall(indFeat)) ;
    F1_avg = 2 * pr_avg * re_avg / ( pr_avg + re_avg ) ;
    fprintf('Average Precision: -- %d\n',pr_avg)
    fprintf('Average Recall: -- %d\n',re_avg)
    fprintf('Average F1 score: -- %d\n',F1_avg)
end

toc


% Automatic closure of multiple sessions
if matlabpool('size')>0
    matlabpool close
end


% Save results
save( fullfile(seqpath,strcat('results_exBMVC11.mat')) ) ;

