% Copyright 2014 Francesco Setti and Marco Cristani
% Department of Computer Science
% University of Verona
% http://www.di.univr.it/
% 
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, subject to the
% following conditions: 
%  * The above copyright notice and this permission notice shall be
%    included in all copies or substantial portions of the Software. 
%  * The Software is provided "as is", without warranty of any kind.
%
% September 2014
%

function [group, score, center] = ff_greedyopt(acc_final,acc_label_mat,o_features,grid_pos,param)


%% FIND GROUPS

% Clear all the cells with only one individual voting
acc_final(sum(acc_label_mat,3)<2) = 0 ;

% Initialize individuals into play
into_play = unique(o_features(:,1)) ;
list_subj = unique(o_features(:,1)) ;

rad = param.radius ;
portion = param.empty ;

% Initialize the group index
idxGroup = 1 ;

% Initialize the condition to exit the while loop
exitcond = 1 ;

while exitcond
    
    % Find the subjects that are not into_play (not necessary the same as
    % others, since list_subj could also have people not detected in the
    % scene). Then, clean all the votes from these subjects from acc_final.
    offside = setdiff(list_subj,into_play) ;
    [~,~,ss] = intersect(offside,o_features(:,1)) ;
    acc_final(sum(acc_label_mat(:,:,ss),3)~=0) = 0 ;
    
    % If acc_final is full of zeros, exit the while loop 
    if sum(acc_final(:)) == 0
        break
    end
    
    % Find the maximum of acc_final.
    [score_max,index_max] = max(acc_final(:)) ;
    % NOTE: if there are multiple maximum in acc_final, it finds the first
    % one, according to the matlab function 'max'.
    
    % Candidate group is the one who generates the maximum in acc_final.
    %%%group_tmp = unique(acc_label{index_max}) ;
    [ii,jj] = ind2sub(size(acc_final),index_max);
    group_tmp = o_features(find(acc_label_mat(ii,jj,:)),1);

    % Define others as all the subjects not involved in the candidate
    % group.
    others = setdiff(into_play,group_tmp) ;
    
    % The group is OK if no other subjects are inside a circle of radious
    % rad*portion and center in the candidate group's center. If the group
    % is not OK, the value in the accumulator will be set as 0 and go to
    % the next iteration.
    if ~isempty(others)
        [~,~,ss] = intersect(others,o_features(:,1)) ;
        if min(pdist2(grid_pos(index_max,:),o_features(ss,2:3))) <= rad*portion
%         if min(pdist2(grid_pos(index_max,:),o_features(:,2:3))) <= rad*portion
            acc_final(index_max) = 0 ;
        else
            into_play = others ;
            group{idxGroup}    = group_tmp;
            score(idxGroup)    = score_max ;
            center(idxGroup,:) = grid_pos(index_max,:) ;
            idxGroup = idxGroup + 1;
        end
    else
        exitcond = 0 ;
        into_play = others;
        group{idxGroup}    = group_tmp;
        score(idxGroup)    = score_max ;
        center(idxGroup,:) = grid_pos(index_max,:) ;
        idxGroup = idxGroup + 1;
    end
    % NOTE: in this condition we could also take into account
    % self-intrusion. It should not occur, but the probability increase if
    % portion is high (~1) and/or covar is high.
    
end

if ~exist('group','var')
    group  = [] ;
    score  = [] ;
    center = [] ;
end

